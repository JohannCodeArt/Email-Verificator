import re

patron = '([a-z]+\S?)[0-9\w]{4,15}\@gmail.com|([a-z]+\S?)[0-9\w]{4,15}\@uip.pa|([a-z]+\S?)[0-9\w]{4,15}\@uip.edu.pa|([a-z]+\S?)[0-9\w]{4,15}\@outlook.es|([a-z]+\S?)[0-9\w]{4,15}\@yahoo.com'

opc = "S"

while opc == "S":
    if __name__ == "__main__":
        nombre = input("Coloque un correo con por lo menos 4 caracteres y con un maximo de 15 caracteres Ej. eljohann123@gmail.com: ")
        if re.match(patron, nombre) is not None:
           print('El Correo es valido')
        else:
           print('El Correo que usted entro no es valido')

    opc = input("Desea verificar otro e-mail? S/N \n").upper()
    if opc == "N":
        exit()
    else:
        pass
